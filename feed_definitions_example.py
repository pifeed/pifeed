#!/usr/bin/env python
# -*- coding: utf-8 -*-

unit_feed_filter = {
   '1':("dagens")
}

unit_screens = {
   '1':(1920,1080),  # first screen
}

#
# Global RSS Feed definitions.
# Name, URL to RSS, Background, How long (in seconds) to display each node, How many loops for feed
#

all_rss_feeds = [
    ("dagens", "Dagens Medicin", "https://www.dagensmedicin.se/rss/", "resources/backgrounds/med112.jpg",17,2)
]

my_default_background = "resources/backgrounds/bg4.jpg"
